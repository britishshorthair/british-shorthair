Equipping Your Home for A British Shorthair Cat
British Shorthair is one of the most popular breeds of cat most especially in the United Kingdom. Despite its large size, this cat is easy going and very affectionate making it a good companion and family pet. 
If you are planning to bring a kitten to your home, there are lots of important things that you need to take into consideration to ensure that it will grow healthy and live comfortably and happily. we will show you everything you need to know when it comes to equipping your home. 
Litter Boxes
Just like any other breeds, a shorthair kitten needs a litter box, a urine and feces collection box for cats and other pets. This equipment is usually given to pets who are allowed to roam around a home freely or do not go outside to excrete their waste. 
If you are planning to get a small cat, it is okay to buy a cheap and smaller litter box because you will be replacing it soon once your cat grows. 
However, we highly recommend that you buy two litter boxes even though you are only bringing home one cat; one should be a plus-sized while the other should accommodate its current size.
Food Dishes and Water Fountains 
Cats also need food and water containers. However, when you’re buying such stuff, it is best that you invest in ceramic, this is essential particularly if you are bringing home a kitten. Plastic contains can make small pits in which molds, viruses, and bacteria can hide. So to avoid this, use ceramic food and water container and clean them on a regular basis. 
On the other hand, when your cat is already big, you should invest in a water fountain that has a pump that is circulating. 
Keep in mind that all cats need to drink plenty of water so that they will be able to fight any disease including urinary tract infection and kidney problems. 
https://britishshorthair.company.com/

